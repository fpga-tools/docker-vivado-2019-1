# Docker Vivado 2019.1
Docker Ubuntu environment that has all dependencies installed to run Vivado 2019.1.

## Notes
Vivado 2019.1 is not installed with this image. It must be installed outside of this image and passed through using the `--volume` switch like so:
```
docker run --volume <local_vivado_path>:/mnt/efs/vivado_2019_1 -it registry.gitlab.com/fpga-tools/docker-vivado-2019-1:latest
```
Note that the mount point `/mnt/efs/vivado_2019_1` is fixed within the Dockerfile.