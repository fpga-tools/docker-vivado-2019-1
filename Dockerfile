# Install all package dependencies for Xilinx tools
FROM "ubuntu:18.04"

# Update Debian package manager
RUN apt-get update

# Set Time Zone
ENV TZ="America/Los_Angeles"
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install tzdata
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN dpkg-reconfigure --frontend noninteractive tzdata

# Run updates and install necessary packages
RUN apt-get install -y \
  wget \
  curl \
  unzip \
  build-essential \
  libglib2.0-0 \
  libsm6 \
  libxi6 \
  libxrender1 \
  libxrandr2 \
  libfreetype6 \
  libfontconfig \
  git \
  fxload \
  bison \
  flex \
  chrpath \
  socat \
  texinfo \
  zlib1g-dev \
  gcc-multilib \
  lib32ncurses5-dev \
  libcanberra-gtk-module \
  u-boot-tools \
  nfs-common \
  xvfb \
  x11vnc \
  xxd \
  tree

# Installs after previous dependencies have completed
RUN apt-get install -y gcc-aarch64-linux-gnu

# Set up Xilinx environment
RUN mkdir -p /mnt/efs
RUN echo 'source /mnt/efs/vivado_2019_1/Vivado/2019.1/settings64.sh' >> ~/.bashrc
